# Sobre
Projeto de sistema acadêmico para demonstração  
de desenvolvimento Java desktop.
 
# Ferramentas Utilizadas 
 
 - Eclipse IDE 2020-03 
 - Maven 
 - Hibernate ORM 5.4.17  
 - Jpa 2.2 
 - Banco de dados h2 
 - Flat Look and Feel 0.36 
 - C3p0 Hibernate ( para pool de conexões ) 
 
# Features 
  
 - Splash Screen com barra de progresso.
 - Trava de arquivo ( lock file ), contra execução de múltiplas instâncias; 
 - Formulários de componentes Swing ( guias, campos de texto, campos de texto com formatação 
 botões de rádio combo-box e tabelas )
 - Gerenciamento de eventos em tabelas, item de menu pop-up e botões. 
 
  

# Estrutura do projeto 
	
	```
	. 
	├── bin 
	│   ├── pom.xml 
	│   ├── src 
	│   └── target 
	├── pom.xml 
	├── README.md 
	├── src 
	│   ├── main 
	│   └── test 
	└── target 
	    ├── cadastro-0.0.1-SNAPSHOT.jar 
	    ├── classes 
	    ├── dependency 
	    ├── generated-sources 
	    ├── generated-test-sources 
	    ├── lib 
	    ├── maven-archiver 
	    ├── maven-status 
	    └── test-classes
	    
	  ```   
	 
# Recomendações
 
- Baixe e descompacte Eclipse ide 2020 --- [link](https://www.eclipse.org/downloads/download.php?file=/oomph/epp/2020-06/R/eclipse-inst-linux64.tar.gz) 
- Baixe o projeto e descompacte - o em um diretório de sua preferência. 
- Importe o projeto para ide. 
- Na ide, com o projeto aberto, clique com o botão direito do mouse  
sobre o mesmo aponte para **Maven** e clique em **Update Project...**   



    
# Suporte
 - Não há suporte. 
 - O sistema pode ser modificado e utilizado de acordo com a licença GNU GPLv3.   
   
# Licença
## GNU GPLv3
   [veja detalhes da licença](https://www.gnu.org/licenses/quick-guide-gplv3.pt-br.html)

 
  
 
