package cadastro.service;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;

import javax.swing.JFormattedTextField;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import cadastro.modelo.Professor;
import cadastro.modelo.Turma;
import cadastro.repositorio.TurmaRepositorio;
import cadastro.util.LogUtil;

public class TurmaService {
	
	private static String className = TurmaService.class.getName();
	private static Professor professor;
	private static Turma turma;
	private static TurmaRepositorio operacoes;
		
	public static boolean salvarTurma(JTextField campoTextoSala, JFormattedTextField campoDataAbertura,
			JFormattedTextField campoDataEncerramento, JTextField campoNomeProfessor,
			JTextField campoTituloProfessor, JTextField campoCurso, JTextField campoCodigo) {
		
		boolean valorRetorno = false;
		
		String sala = campoTextoSala.getText().trim();
		String dataAbertura = campoDataAbertura.getText().trim();
		String dataEncerramento = campoDataEncerramento.getText().trim();
		String nomeProfessor = campoNomeProfessor.getText().trim();
		String tituloProfessor = campoTituloProfessor.getText().trim();
		String curso = campoCurso.getText().trim();
		
		if(sala.equals("") || dataAbertura.equals("") 
				|| dataEncerramento.equals("") || nomeProfessor.equals("") 
				|| tituloProfessor.equals("") || curso.equals(""))
		{
			JOptionPane.showMessageDialog(null,
					"Favor preencher todos os campos antes de prosseguir", "Aviso",
					JOptionPane.WARNING_MESSAGE);
			
		}else {
			// formada a data de string para date
			DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
			
			Date abertura = null, fechamento = null;
			
			try {
				abertura = (Date) df.parse(campoDataAbertura.getText().trim());
				fechamento = (Date) df.parse(campoDataEncerramento.getText().trim());
			} catch (ParseException e) {
				LogUtil.setApplicationLog(className, Level.WARNING, e.getMessage());
			}
			
			/*instancia entidades*/
			professor = new Professor();
			professor.definirNome(campoNomeProfessor.getText().trim());
			professor.setTitulacao(campoTituloProfessor.getText().trim());
			
			turma = new Turma();
			turma.setCurso(curso);
			turma.definirProfessor(professor);
			turma.setDataAbertura(abertura);
			turma.setDataEncerramento(fechamento);
			turma.setSala( campoTextoSala.getText().trim() );
					
			
			// instancia camada dao e salva
			operacoes = new TurmaRepositorio();
			boolean estaSalvo = operacoes.salvarTurma(turma);
			// exibe mensagem
			if(estaSalvo) {
				valorRetorno = estaSalvo;
				JOptionPane.showMessageDialog(null,
						"Turma salva com sucesso", "Sucesso",
						JOptionPane.INFORMATION_MESSAGE);
				
			}else {
				JOptionPane.showMessageDialog(null,
						"Erro ao salvar o registro o mesmo ja existe na base de dados", "Falha",
						JOptionPane.ERROR_MESSAGE);
				limparFormulario(campoTextoSala, campoNomeProfessor,
						campoTituloProfessor, campoDataEncerramento,
						campoDataAbertura, campoCurso, campoCodigo);
				
			}
			
		}
		
		return valorRetorno;
		
		
	}
	
	public static boolean atualizarTurma(JTextField campoTextoSala, JFormattedTextField campoDataAbertura,
			JFormattedTextField campoDataEncerramento, JTextField campoNomeProfessor,
			JTextField campoTituloProfessor, JTextField campoCurso, JTextField campoCodigo) {
		
		boolean valorRetorno = false;
		
		String codigo = campoCodigo.getText().trim();
		String sala = campoTextoSala.getText().trim();
		String dataAbertura = campoDataAbertura.getText().trim();
		String dataEncerramento = campoDataEncerramento.getText().trim();
		String nomeProfessor = campoNomeProfessor.getText().trim();
		String tituloProfessor = campoTituloProfessor.getText().trim();
		String curso = campoCurso.getText().trim();
		
		if(sala.equals("") || dataAbertura.equals("") 
				|| dataEncerramento.equals("") || nomeProfessor.equals("") 
				|| tituloProfessor.equals("") || curso.equals("")
				|| codigo.equals(""))
		{
			JOptionPane.showMessageDialog(null,
					"Favor preencher todos os campos antes de prosseguir com a atualização", "Aviso",
					JOptionPane.WARNING_MESSAGE);
			
		}else {
			// formada a data de string para date
			DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
			
			Date abertura = null, fechamento = null;
			
			try {
				abertura = (Date) df.parse(campoDataAbertura.getText().trim());
				fechamento = (Date) df.parse(campoDataEncerramento.getText().trim());
			} catch (ParseException e) {
				LogUtil.setApplicationLog(className, Level.WARNING, e.getMessage());
			}
			
			/*instancia entidades*/
			professor = new Professor();
			professor.definirNome(campoNomeProfessor.getText().trim());
			professor.setTitulacao(campoTituloProfessor.getText().trim());
			
			turma = new Turma();
			turma.setCodigo(codigo);
			turma.setCurso(curso);
			turma.definirProfessor(professor);
			turma.setDataAbertura(abertura);
			turma.setDataEncerramento(fechamento);
			turma.setSala( campoTextoSala.getText().trim() );
					
			
			// instancia camada dao e salva
			operacoes = new TurmaRepositorio();
			boolean estaAtualizado = operacoes.atualizarTurma(turma);
			// exibe mensagem
			if(estaAtualizado) {
				valorRetorno = true;
				JOptionPane.showMessageDialog(null,
						"Turma atualizada com sucesso", "Sucesso",
						JOptionPane.INFORMATION_MESSAGE);
				
			}else {
				JOptionPane.showMessageDialog(null,
						"Erro ao atualizar dados da turma", "Falha",
						JOptionPane.ERROR_MESSAGE);
				limparFormulario(campoTextoSala, campoNomeProfessor,
						campoTituloProfessor, campoDataEncerramento,
						campoDataAbertura, campoCurso, campoCodigo);
				
			}
			
		}
		
		return valorRetorno;
	}
	
	public static void limparFormulario(JTextField campoTextoSala, JTextField campoNomeProfessor,
			JTextField campoTituloProfessor, JFormattedTextField campoDataEncerramento,
			JFormattedTextField campoDataAbertura, JTextField campoCurso, JTextField campoCodigo) {
		campoCodigo.setText(null);
		campoCurso.setText(null);
		campoTextoSala.setText(null);
		campoNomeProfessor.setText(null);
		campoTituloProfessor.setText(null);
		campoDataEncerramento.setText(null);
		campoDataAbertura.setText(null);		
	}
	
	public static List<Turma> carregarTurmas() {
		List<Turma> valorRetorno = new ArrayList<Turma>();
		
		if(operacoes == null)
			operacoes = new TurmaRepositorio();
		
		valorRetorno = operacoes.carregarTurmas();
		
		return valorRetorno;
	}
	
	public static boolean excluirTurma(Turma turma) {
		boolean valorRetorno = false;
		// validar objeto turma 
		if(
			turma.getCodigo().equals("") &&
			turma.getCurso().equals("") &&
			turma.getProfessor() == null &&
			turma.getDataAbertura() == null &&
			turma.getDataEncerramento() == null &&
			turma.getSala().equals("") ) {
			JOptionPane.showMessageDialog(
					null,
					"Favor Preencher todos os dados antes de prosseguir",
					"aviso",
					JOptionPane.PLAIN_MESSAGE);
			return valorRetorno;
		}else {
			// dao
			if(operacoes == null)
				operacoes = new TurmaRepositorio();
			
			valorRetorno = operacoes.deletarTurma(turma.getCodigo());
			 			
		}
		return valorRetorno;
	}
	
	public static Turma obterTurma(String curso, String professor,
			Date dataInicioCurso) {
		
		Turma valorRetorno = null;			
		
		if(operacoes == null)
			operacoes = new TurmaRepositorio();
		
		valorRetorno = operacoes.obterTurma(curso, professor, dataInicioCurso);
		
		return valorRetorno;		
	}
	
	public static boolean atualizarTurmaComAluno( Turma turma ) {
		
		boolean valorRetorno = false;
		
		if(operacoes == null)
			operacoes = new TurmaRepositorio();
		
		valorRetorno = operacoes.atualizarTurmaComAlunos( turma );
		
		return valorRetorno;
	}
	
	public static Turma buscarTurmaPorId( String id ) {
		Turma valorRetorno = null;

		if(operacoes == null)
			operacoes = new TurmaRepositorio();
		
		valorRetorno = operacoes.obterTurmaPorId( id );
		
		return valorRetorno;
	}
}
