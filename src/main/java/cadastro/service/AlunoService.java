package cadastro.service;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;

import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.text.DefaultFormatterFactory;
import javax.swing.text.MaskFormatter;

import cadastro.janela.LetraMaiuscula;
import cadastro.modelo.Aluno;
import cadastro.modelo.TipoDocumento;
import cadastro.modelo.Turma;
import cadastro.repositorio.AlunoRepositorio;
import cadastro.util.LogUtil;

public class AlunoService {
	
	private static AlunoRepositorio operacoes = null;
		
	public static boolean salvarAluno(
			JTextField campoCodigoAluno,
			JTextField campoNomeAluno,
			JTextField campoEndereco,
			JRadioButton botaoSelecionado,
			JFormattedTextField campoDocumento,
			JFormattedTextField campoTelefone,
			String turmaSelecionada) 
	{
		boolean valorRetorno = false;
	
		// verificar se todos os dados estao preeenchidos
		if( campoNomeAluno.getText().equals("") 
				|| campoEndereco.getText().equals("") 
				|| campoDocumento
				.getText().equals("                                     -  ")
				|| campoDocumento.getText().equals("  .   .   - ")
				|| campoTelefone.getText().equals("(  )      -    ")
				|| turmaSelecionada.equals("")
				|| botaoSelecionado == null 
				|| botaoSelecionado.getText().equals("")) {
			
			JOptionPane.showMessageDialog(
					null,
					"Favor preencher todos os campos do formulário",
					"aviso",
					JOptionPane.WARNING_MESSAGE);
		} else {
			
			/* verificar existência de código para 
			   determinar se salva ou atualiza */
			
			if( !campoCodigoAluno.getText().equals("") ) {
			
				// prepara dados do aluno para atualização
				Aluno aluno = new Aluno();
				
				aluno.definirMatricula(
					Integer
					.valueOf( campoCodigoAluno.getText().trim() )
				);
				
				aluno.definirNome( campoNomeAluno.getText() );
				
				aluno.setEndereco( campoEndereco.getText() );
				
				aluno.setTelefone( campoTelefone.getText() );
				
				if ( botaoSelecionado.getText()
						.equals("Certidão nasc.") )
					
					aluno.setTipoDocumento(
							TipoDocumento.CERTIDAO_NASCIMENTO );
				
				else
					aluno.setTipoDocumento(
							TipoDocumento.RG );
				
				aluno.setDocumento(
						campoDocumento.getText().toUpperCase() );
				
				
				if ( botaoSelecionado.getText().equals("Certidão nasc.") )
					aluno.setTipoDocumento( TipoDocumento.CERTIDAO_NASCIMENTO );
				else
					aluno.setTipoDocumento( TipoDocumento.RG );
						
				
				// remove aluno da turma onde  anterioriormente estava matriculado
				List<Turma> turmas = TurmaService.carregarTurmas();
				
				boolean estaRemovidoDaTurma = false;
				
				for(Turma turma: turmas) {
					
					List<Aluno> alunos = turma.getAlunos();
					
					for(Aluno busca: alunos) {
						
						if (busca.obterMatricula()
								.equals(aluno.obterMatricula())) {
							
							turma.excluirAluno(busca);
							
							estaRemovidoDaTurma = TurmaService
									.atualizarTurmaComAluno( turma );
							
							break;
						}
					}
				}
				
				
				// atualiza os dados do aluno no bd
				if ( operacoes == null)
					operacoes = new AlunoRepositorio();
								
				boolean alunoAtualizado = operacoes.atualizarDadosAluno( aluno ); 
				
				// obter nova turma do aluno no banco de dados
				String[] dadosTurma = turmaSelecionada.split(";");
				String curso = dadosTurma[0].trim();
				String professor = dadosTurma[1].trim();
				DateFormat formatadorData = new SimpleDateFormat("dd/MM/yyyy");
				Date dataInicioCurso = null;
				try {
					
					dataInicioCurso = ( Date ) formatadorData
							.parse( dadosTurma[2].trim() );
					
				} catch (ParseException e) {
					
					LogUtil.setApplicationLog(
							"AlunoService",
							Level.WARNING,
							e.getMessage());
					
				}
				
				// obtem a turma ao qual o aluno deseja se matricular
				Turma turma = TurmaService.obterTurma(curso, professor, dataInicioCurso);
				boolean estaAtualizadaTurma = false;
				
				if ( turma != null) {
					
					turma.incluirAluno( aluno );
					estaAtualizadaTurma = TurmaService
							.atualizarTurmaComAluno( turma );
				}
				
				if ( estaRemovidoDaTurma && alunoAtualizado && estaAtualizadaTurma ) {
					valorRetorno = true;
					JOptionPane.showMessageDialog(
							null,
							"Dados atualizados com sucesso!",
							"aviso",
							JOptionPane.INFORMATION_MESSAGE);
				} else {
					JOptionPane.showMessageDialog(
							null,
							"Falha ao atualizar dados do aluno!",
							"aviso",
							JOptionPane.ERROR_MESSAGE);
				}
				
			} else {
				
				// salva objeto no banco de dados
				Aluno aluno = new Aluno();
				
				aluno.definirNome( campoNomeAluno.getText() );
				
				aluno.setEndereco( campoEndereco.getText() );
				
				aluno.setTelefone( campoTelefone.getText() );
				
				if ( botaoSelecionado.getText().equals("Certidão nasc.") )
					aluno.setTipoDocumento( TipoDocumento.CERTIDAO_NASCIMENTO );
				else
					aluno.setTipoDocumento( TipoDocumento.RG );
				
				aluno.setDocumento( campoDocumento.getText().toUpperCase() );
				
				// obter turma do banco de dados
				String[] dadosTurma = turmaSelecionada.split(";");
				String curso = dadosTurma[0].trim();
				String professor = dadosTurma[1].trim();
				DateFormat formatadorData = new SimpleDateFormat("dd/MM/yyyy");
				Date dataInicioCurso = null;
				try {
					
					dataInicioCurso = ( Date ) formatadorData
							.parse( dadosTurma[2].trim() );
					
				} catch (ParseException e) {
					
					LogUtil.setApplicationLog(
							"AlunoService",
							Level.WARNING,
							e.getMessage());
					
				}
				
				// salvar o aluno no banco de dados
				if ( operacoes == null)
					operacoes = new AlunoRepositorio();
				
				/*o aluno retorna com matricula*/
				Aluno alunoSalvo = operacoes.salvarAluno( aluno ); 
				
				Turma turma = TurmaService.obterTurma(curso, professor, dataInicioCurso);
				
				boolean estaAtualizadaTurma = false;
				
				if ( turma != null && alunoSalvo != null ) {
					
					turma.incluirAluno( alunoSalvo );
					estaAtualizadaTurma = TurmaService
							.atualizarTurmaComAluno( turma );
				}
				
				
				if ( estaAtualizadaTurma ) {
					
					JOptionPane.showMessageDialog(
							null,
							"Dados salvos com sucesso!",
							"aviso",
							JOptionPane.INFORMATION_MESSAGE);
					
					valorRetorno = true;
					
				} else {
					
					JOptionPane.showMessageDialog(
							null,
							"Falha ao salvar os dados do aluno!",
							"aviso",
							JOptionPane.ERROR_MESSAGE);
				}
				
				
			}
			
			
		}
		
		operacoes = null;
		return valorRetorno;
		
	}	 
	
	public static boolean removerAluno( int matricula ) {
		
		boolean valorRetorno = false;
		
		if ( operacoes == null )
			operacoes = new AlunoRepositorio();
		
		valorRetorno =  operacoes.removerAluno( matricula );
		
		operacoes = null;
		
		return valorRetorno;
	}
	 
	public static void limparFormularioAluno (
			JTextField campoCodigo,
			JTextField campoNomeAluno,
			JTextField campoEndereco,
			JFormattedTextField campoTelefone,
			JRadioButton rdbCertidao,
			JRadioButton rdbRG,
			JFormattedTextField campoDocumento,
			JComboBox<String> cmbTurmas,
			MaskFormatter formatador,
			MaskFormatter formatador1,
			MaskFormatter formatadorTelefone,
			MaskFormatter formatadorRg,
			MaskFormatter formatadorCertidaoNascimento,
			JFrame janela)
	{
		campoCodigo.setText( null );
		campoNomeAluno.setText( null );
		campoEndereco.setText( null );
		campoTelefone.setText( null );
		rdbCertidao.setSelected( true );
		rdbRG.setSelected( false );
		campoDocumento.setText( null );
		campoDocumento.setDocument( new LetraMaiuscula() );
		cmbTurmas.setSelectedIndex(0);
		
		if(rdbCertidao.isSelected()) {
			
			campoDocumento.setText("");
			
			campoDocumento.setFormatterFactory(
					new DefaultFormatterFactory(
							formatadorCertidaoNascimento));
			
			campoDocumento.setText(null);
			
			//janela.repaint();
			
		}
	}
	
	public static boolean removerAlunoDaTurma ( int matricula ) {
		// remove aluno da turma onde estava matriculado
		List<Turma> turmas = TurmaService.carregarTurmas();
		
		boolean estaRemovidoDaTurma = false;
		
		for(Turma turma: turmas) {
			
			List<Aluno> alunos = turma.getAlunos();
			
			for(Aluno busca: alunos) {
				
				if (busca.obterMatricula()
						.equals( matricula )) {
					
					turma.excluirAluno(busca);
					
					estaRemovidoDaTurma = TurmaService
							.atualizarTurmaComAluno( turma );
					
					break;
				}
			}
		}
		
		return estaRemovidoDaTurma;
	}
}
