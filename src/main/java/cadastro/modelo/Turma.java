package cadastro.modelo;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;

import cadastro.util.LogUtil;

@Entity 
public class Turma implements Iturma{
	
	/**
	 * esta estrategia de primary key gera erro no eclipse
	 * para resolver:
	 *  
	 * Window->
	 * -  Preferences->
	 *  -   Java Persistence->
	 * 	 -	JPA->
	 * 	  -  Errors/Warnings->
	 * 	   - Queries and Generators [modificar] “Generator is not defined in the persistence unit”
	 * 		- mudar valor para warning ou ignore
	 * 
	 * */
	
	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid2")
	private String codigo;
	
	private String curso;
	
	private String sala;
	
	@Temporal(TemporalType.DATE)
	private Date dataAbertura;
	
	@Temporal(TemporalType.DATE)
	private Date dataEncerramento;
	
	
	@OneToMany(cascade = CascadeType.ALL)	
	private List<Aluno> alunos = new ArrayList<Aluno>();
		
	@OneToOne(cascade = CascadeType.ALL) 
	private Professor professor;

	@Transient
	private String dataAberturaTexto;
	@Transient
	private String dataFechamentoTexto;
	
	@Override
	public boolean estaAberta() {
	
		boolean valorRetorno = false;
		
		// 1 dia convertido em milisegundos
		long miliSegundosPorDia = 1000 * 60 * 60 * 24;
		
		long dataInicioTurma = this.dataAbertura.getTime();
		
		// data de inicio da turma mais 10 dias (já que não foi
		// especificado nos requisitos) para periodo de inscricao 
		long periodoInscricao = dataInicioTurma + (miliSegundosPorDia*10);
		
		// data atual
		long hoje = new Date().getTime();
		
		// compara se data atual esta dentro do periodo de inscricao
		if(hoje <= periodoInscricao)
			valorRetorno = true;
		
		return valorRetorno;
	}

	@Override
	public void definirProfessor(Professor professor) {
		this.professor = professor;
		
	}

	@Override
	public void incluirAluno(Aluno aluno) {
		this.alunos.add(aluno);		
	}
	
	@Override
	public void excluirAluno(Aluno aluno) {
		this.alunos.remove( aluno );		
	}
	
	public String getCodigo() {
		return codigo;
	}

	public String getSala() {
		return sala;
	}

	public Date getDataAbertura() {
		return dataAbertura;
	}

	public Date getDataEncerramento() {
		return dataEncerramento;
	}

	public List<Aluno> getAlunos() {
		return alunos;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public void setSala(String sala) {
		this.sala = sala;
	}

	public void setDataAbertura(Date dataAbertura) {
		this.dataAbertura = dataAbertura;
	}

	public void setDataEncerramento(Date dataEncerramento) {
		this.dataEncerramento = dataEncerramento;
	}

	public void setAlunos(List<Aluno> alunos) {
		this.alunos = alunos;
	}

	public Professor getProfessor() {
		return this.professor;
	}

	public String getCurso() {
		return curso;
	}

	public void setCurso(String curso) {
		this.curso = curso;
	}
	
	public String obterDataAberturaConvertida(Date date) {
		DateFormat formatador = new SimpleDateFormat("dd/MM/yyyy");
		try {
			dataAberturaTexto = formatador.format(date);
		} catch (Exception e) {
			LogUtil.setApplicationLog(Turma.class.getName(),
					Level.WARNING, e.getMessage());
		}
		return dataAberturaTexto;
	}
	
	public String obterDataFechamentoConvertida(Date date) {
		DateFormat formatador = new SimpleDateFormat("dd/MM/yyyy");
		try {
			dataFechamentoTexto = formatador.format(date);
		} catch (Exception e) {
			LogUtil.setApplicationLog(Turma.class.getName(),
					Level.WARNING, e.getMessage());
		}
		return dataFechamentoTexto;
	}

	
}
