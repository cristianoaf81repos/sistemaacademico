package cadastro.modelo;

public interface Usuario {
	void definirNome(String nome);
	String obterNome();
	void definirMatricula(Integer numeroMatricula);
	Integer obterMatricula();
}
