package cadastro.modelo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Professor implements Usuario{
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)	
	private Integer matricula;
	private String nome;
	private String titulacao;
			
	public Professor() {}

	@Override
	public void definirNome(String nome) {
		this.nome = nome;
		
	}
	
	@Override
	public String obterNome() {
		return this.nome;
	}
	
	@Override
	public void definirMatricula(Integer numeroMatricula) {
		this.matricula = numeroMatricula;
	}
	
	@Override
	public Integer obterMatricula() {
		return this.matricula;
	}
	
	
	public String getTitulacao() {
		return titulacao;
	}
		
		
	public void setTitulacao(String titulacao) {
		this.titulacao = titulacao;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((matricula == null) ? 0 : matricula.hashCode());
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		result = prime * result + ((titulacao == null) ? 0 : titulacao.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Professor other = (Professor) obj;
		if (matricula == null) {
			if (other.matricula != null)
				return false;
		} else if (!matricula.equals(other.matricula))
			return false;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		if (titulacao == null) {
			if (other.titulacao != null)
				return false;
		} else if (!titulacao.equals(other.titulacao))
			return false;
		return true;
	}

	
	
}
