package cadastro.modelo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Aluno implements Usuario{
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)	
	private Integer matricula;
		
	private String nome;
	
	private String endereco;
	
	private String telefone;
	
	@Enumerated(EnumType.STRING)
	private TipoDocumento tipoDocumento;
	
	@Column(unique = true)
	private String documento;
			
	public String getEndereco() {
		return endereco;
	}

	public String getTelefone() {
		return telefone;
	}

	public TipoDocumento getTipoDocumento() {
		return tipoDocumento;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public void setTipoDocumento(TipoDocumento tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}
	

	public String getDocumento() {
		return documento;
	}

	public void setDocumento(String documento) {
		this.documento = documento;
	}
		
	@Override
	public void definirNome(String nome) {
		this.nome = nome;		
	}
	
	@Override
	public String obterNome() {
		return this.nome;
	}
	
	@Override
	public void definirMatricula(Integer numeroMatricula) {
		this.matricula = numeroMatricula;
		
	}
	
	@Override
	public Integer obterMatricula() {
		return this.matricula;
	}

	public Aluno() {}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((documento == null) ? 0 : documento.hashCode());
		result = prime * result + ((endereco == null) ? 0 : endereco.hashCode());
		result = prime * result + ((matricula == null) ? 0 : matricula.hashCode());
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		result = prime * result + ((telefone == null) ? 0 : telefone.hashCode());
		result = prime * result + ((tipoDocumento == null) ? 0 : tipoDocumento.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Aluno other = (Aluno) obj;
		if (documento == null) {
			if (other.documento != null)
				return false;
		} else if (!documento.equals(other.documento))
			return false;
		if (endereco == null) {
			if (other.endereco != null)
				return false;
		} else if (!endereco.equals(other.endereco))
			return false;
		if (matricula == null) {
			if (other.matricula != null)
				return false;
		} else if (!matricula.equals(other.matricula))
			return false;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		if (telefone == null) {
			if (other.telefone != null)
				return false;
		} else if (!telefone.equals(other.telefone))
			return false;
		if (tipoDocumento != other.tipoDocumento)
			return false;
		return true;
	}	
	
}
