package cadastro.modelo;

public interface Iturma {
	boolean estaAberta();
	void definirProfessor(Professor professor);
	void incluirAluno(Aluno aluno);
	void excluirAluno(Aluno aluno);
}
