package cadastro.principal;

import java.io.File;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.nio.file.StandardOpenOption;
import java.text.ParseException;
import java.util.logging.Level;

import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import cadastro.janela.JanelaPrincipal;
import cadastro.util.InicializacaoUtil;
import cadastro.util.LogUtil;
import cadastro.util.LookAndFeelUtil;

public class PrincipalClass {

	public static void main(String[] args) throws IOException {
		
		// usado no log
		String className = PrincipalClass.class.getName();
		// usado para criar lock
		String userHome = System.getProperty("user.home");		

		
		iniciarJanelaPrincipal(className, userHome);
	}

	private static void iniciarJanelaPrincipal(String className, String userHome)
			throws IOException {
		
		String systemOsName = System.getProperty("os.name").toLowerCase();
		File arquivoTrava = null;
		
		if ( systemOsName.contains("win") ) {
			arquivoTrava = new File(userHome,"cadastro.lock");
			Process p = Runtime
					.getRuntime()
					.exec("attrib +H " + arquivoTrava.getPath());
			try {
				p.waitFor();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		if ( systemOsName.contains("nix") || systemOsName.contains("nux") ) {
			arquivoTrava = new File(userHome,"."+"cadastro.lock");			
		}
		
		if ( systemOsName.contains("mac") ) {
			arquivoTrava = new File(userHome,"cadastro.lock");
			Process p = Runtime
					.getRuntime()
					.exec("chflags hidden " + arquivoTrava.getPath());
			try {
				p.waitFor();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		
		try {
			
			FileChannel canalArquivo = FileChannel.open(
					arquivoTrava.toPath(),
					StandardOpenOption.CREATE,
					StandardOpenOption.WRITE);
			
			FileLock trava = canalArquivo.tryLock();
			
			if ( trava == null ) {
				
				// seta o look and feel
				LookAndFeelUtil.definirVisualdaAplicação();
				
				JOptionPane.showMessageDialog(
						null,
						"Outra instância do programa já está em execução",
						"erro",
						JOptionPane.ERROR_MESSAGE);
				
			} else {
				
				// mostra a splashScreen e inicializa 
				// banco de dados
				InicializacaoUtil.mostrarLogoNoInicio();
				
				SwingUtilities.invokeLater( () -> {
					
					JanelaPrincipal janela;
					
					try {
						
						janela = JanelaPrincipal.Singleton();
						
						janela.setVisible(true);
						
					} catch (ParseException e) {
						
						LogUtil.setApplicationLog(
								className,
								Level.WARNING,
								e.getMessage());
					}
					
				} );
				
			}
		} catch (Exception errorMessage) {
			LogUtil.setApplicationLog(
					className,
					Level.WARNING,
					errorMessage.getMessage());
		}

	}
	
	
	
}


