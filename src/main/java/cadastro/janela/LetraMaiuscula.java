package cadastro.janela;

import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

public class LetraMaiuscula extends PlainDocument{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3559419954939648954L;

	@Override
	public void insertString(int offs, String str, AttributeSet a)
			throws BadLocationException {
		
		super.insertString(offs, str.toUpperCase(), a);
	}

	
}
