package cadastro.janela;

import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.stream.Collectors;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.text.DefaultFormatterFactory;
import javax.swing.text.MaskFormatter;

import cadastro.modelo.Professor;
import cadastro.modelo.Turma;
import cadastro.service.AlunoService;
import cadastro.service.TurmaService;
import cadastro.util.LogUtil;


public class JanelaPrincipal extends JFrame {

	static final String RESTRICTION = "restriction";

	/**
	 *  serial e variaveis
	 */	
	private static final long serialVersionUID = -6835270851630276094L;
	
	private JTabbedPane painelCadastro;
	
	private JLabel etiquetaCodigoTurma,
	etiquetaSala,
	etiquetaDataAbertura,
	etiquetaDataEncerramento,
	etiquetaNomeProfessor,
	etiquetaTitulacao,
	etiquetaCurso,
	etiquetaCodigoAluno,
	etiquetaNomeAluno,
	etiquetaEndereco,
	etiquetaTelefone,
	etiquetaTipoDocumento,
	etiquetaDocumento,
	etiquetaTurmaAluno;
	
	private JTextField campoTextoSala,
	campoNomeProfessor,
	campoTituloProfessor,
	campoCurso,
	campoCodigo,
	campoCodigoAluno,
	campoNomeAluno,
	campoEndereco;
	
	private JFormattedTextField campoDataAbertura,
	campoDataEncerramento,
	campoTelefone,
	campoDocumento;
	
	private JPanel pnCadastroTurma ,
	pnCadastroAluno,
	pnBotoes,
	pnRadio,
	pnBotoesAluno;
	
	private List<Turma> turmas;
	
	private MaskFormatter formatador,
	formatador1,
	formatadorTelefone,
	formatadorRg,
	formatadorCertidaoNascimento;
	
	
	private JButton btnSalvar,
	btnLimpar,
	btnAtualizar,
	btnExcluir,
	btnAlunoSalvar,
	btnAlunoLimpar;
	
	private Image imgSalvar,
	imgLimpar,
	imgAtualizar,
	imgDeletar;
	
	private DefaultTableModel modeloTabela;
	private DefaultTableModel modeloTabelaAlunos;
	private JTable tabelaTurmas;
	private JTable tabelaAlunos;
	private JScrollPane painelRolagemTabela;
	private JScrollPane painelRolagemTabelaAlunos;
	private String[] cabecalhoTabela;
	private String[] cabecalhoTabelaAlunos;
	
	private ButtonGroup rdButtonGroup;
	private JRadioButton rdbRG, rdbCertidao;
	private JComboBox<String> cmbTurmas; 
	private String[] cmbTurmasDados;
	
	private JPopupMenu menuExclusaoAluno;
	private JMenuItem itemRemoverAluno;
	private int  linhaSelecionadaTabelaAlunos = -1;
	
	private static JanelaPrincipal INSTANCE = null;
	
	
	public static  JanelaPrincipal Singleton () throws ParseException {
		
		if ( INSTANCE == null )
			INSTANCE = new JanelaPrincipal();
		
		return INSTANCE;
	}
	
	
	/* construtor */
	private JanelaPrincipal() throws ParseException {
						
		/*formatador para campo data usando 2 para evitar bug no linux*/
		formatador = new MaskFormatter("##/##/####");
		formatador1 = new MaskFormatter("##/##/####");
		
		/*formatadores específicos para telefone rg e certidao nascimento*/
		formatadorTelefone = new MaskFormatter("(##) #####-####");
		formatadorRg = new MaskFormatter("##.###.###-*");
		formatadorCertidaoNascimento = 
			new MaskFormatter("###### ## ## #### # ##### ### #######-##"
		);
		
		
		
		/*inicializacao de componentes*/
		inicializarComponentes();		
		
		
		/*tratativa de evento*/
		tratarEventos();
	}
		
	
	
	/* inicializa componentes */
	private void inicializarComponentes() throws ParseException {
		/*
		 * configuracoes tam, op de fechamento, posicionamento da
		 *  janela, layout e titulo
		 *  */
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("Sistema de Cadastro escolar");
		setBounds(0, 0, 840, 540);
		setResizable(false);
		setLayout(null);
		setLocationRelativeTo(null);
		
		this.turmas = new ArrayList<Turma>();
		
		/*define icone da aplicação*/
		setIconImage(
				Toolkit.getDefaultToolkit()
				.getImage("src/main/java/cadastro/splash/icon32.png"));
		
		/*prepara icones dos botoes*/
		try {
			imgSalvar = Toolkit
					.getDefaultToolkit()
					.getImage("src/main/java/cadastro/splash/save.png");
			
			imgLimpar = Toolkit
					.getDefaultToolkit()
					.getImage("src/main/java/cadastro/splash/clean.png");
			
			imgAtualizar = Toolkit
					.getDefaultToolkit()
					.getImage("src/main/java/cadastro/splash/update.png");
			
			imgDeletar = Toolkit
					.getDefaultToolkit()
					.getImage("src/main/java/cadastro/splash/delete.png");
			
		} catch (Exception e) {
			
			LogUtil.setApplicationLog(
					JanelaPrincipal.class.getName(), Level.WARNING, e.getMessage());
		}
		
		
		/*painel cadastro*/		
		painelCadastro = new JTabbedPane();
		painelCadastro.setBounds(5, 15, 830, 260);
		pnCadastroTurma = new JPanel( new GridLayout(6,2,5,15));
		pnCadastroTurma.setBorder(BorderFactory.createTitledBorder("Dados da turma"));
		pnCadastroAluno = new JPanel( new GridLayout(7, 2, 5, 5) );
		pnCadastroAluno.setBorder(BorderFactory.createTitledBorder("Dados do aluno"));
		pnBotoes = new JPanel( new FlowLayout(FlowLayout.LEFT, 5, 5) );
		pnBotoes.setBounds(0, 270, 645, 30);	
		
		/*adiciona items de gui ao painel de turmas*/
		etiquetaCodigoTurma = new JLabel("");
		etiquetaCodigoTurma.setVisible(false);
		campoCodigo = new JTextField();
		campoCodigo.setVisible(false);/*nao visivel*/
		etiquetaSala = new JLabel("Sala:");
		etiquetaDataAbertura = new JLabel("Data Abertura:");
		etiquetaDataEncerramento = new JLabel("Data Encerramento");
		etiquetaNomeProfessor = new JLabel("Professor nome:");
		etiquetaTitulacao = new JLabel("Professor título:");
		etiquetaCurso = new JLabel("Curso/Palestra:");		
		campoTextoSala = new JTextField();
		campoDataAbertura = new JFormattedTextField(formatador);
		campoDataEncerramento = new JFormattedTextField(formatador1);
		campoNomeProfessor = new JTextField();
		campoTituloProfessor = new JTextField();		
		campoCurso = new JTextField();
		cabecalhoTabela = new String[] {"codigo","curso","sala",
				"dt_abertura","dt_encerramento","professor","titulo","situação"};
		modeloTabela = new DefaultTableModel(cabecalhoTabela, 0);
		tabelaTurmas = new JTable(modeloTabela) {
			private static final long serialVersionUID = -5847408408899713721L;
			@Override
			public boolean isCellEditable(int row, int column) {

				return false;
			}			
		};		
		painelRolagemTabela = new JScrollPane(tabelaTurmas);
		painelRolagemTabela.setBounds(5, 305, 830, 205);
		
		/*insere icones nos botes*/
		btnSalvar = new JButton("Salvar",new ImageIcon(imgSalvar));
		btnLimpar = new JButton("Limpar", new ImageIcon(imgLimpar));
		btnAtualizar = new JButton("Atualizar", new ImageIcon(imgAtualizar));
		btnExcluir = new JButton("Excluir",new ImageIcon(imgDeletar));
		
		/*mnemonicos*/
		btnSalvar.setMnemonic(KeyEvent.VK_S);
		btnLimpar.setMnemonic(KeyEvent.VK_L);
		btnAtualizar.setMnemonic(KeyEvent.VK_A);
		btnExcluir.setMnemonic(KeyEvent.VK_E);
		
		/*botoes ocultos ate selecionar item na tabela*/
		btnAtualizar.setVisible(false);
		btnExcluir.setVisible(false);
					
		/*adiciona elementos ao painel cadastro de turma*/
		//pnCadastroTurma.add(etiquetaCodigoTurma);
		pnCadastroTurma.add(campoCodigo);
		
		pnCadastroTurma.add(etiquetaCurso);
		pnCadastroTurma.add(campoCurso);
		
		pnCadastroTurma.add(etiquetaSala);
		pnCadastroTurma.add(campoTextoSala);
		
		pnCadastroTurma.add(etiquetaDataAbertura);
		pnCadastroTurma.add(campoDataAbertura);
		
		
		pnCadastroTurma.add(etiquetaDataEncerramento);
		pnCadastroTurma.add(campoDataEncerramento);
		
		pnCadastroTurma.add(etiquetaNomeProfessor);
		pnCadastroTurma.add(campoNomeProfessor);
		
		pnCadastroTurma.add(etiquetaTitulacao);
		pnCadastroTurma.add(campoTituloProfessor);
		
		
		pnBotoes.add(btnSalvar);
		pnBotoes.add(btnLimpar);
		pnBotoes.add(btnAtualizar);
		pnBotoes.add(btnExcluir);
		
		
		
		/*cria estrutura de guias*/
		painelCadastro.add("Turmas", pnCadastroTurma);		
		
		/*cria items de gui para o painel de cadastro de alunos*/
		etiquetaCodigoAluno = new JLabel("");
		etiquetaCodigoAluno.setVisible(false);
		campoCodigoAluno = new JTextField();
		campoCodigoAluno.setVisible(false);
		etiquetaNomeAluno = new JLabel("Nome:");
		campoNomeAluno = new JTextField();
		etiquetaEndereco = new JLabel("Endereço:");
		campoEndereco = new JTextField();
		etiquetaTelefone = new JLabel("Telefone:");
		campoTelefone = new JFormattedTextField(formatadorTelefone);
		etiquetaTipoDocumento = new JLabel("Tipo de Documento:");
		rdButtonGroup = new ButtonGroup();
		rdbRG = new JRadioButton("Rg");
		rdbCertidao = new JRadioButton("Certidão nasc.");
		rdbCertidao.setSelected(true);
		pnRadio = new JPanel(new FlowLayout(FlowLayout.LEFT, 2, 2));
		pnRadio.add(rdbRG);
		pnRadio.add(rdbCertidao);
		rdButtonGroup.add(rdbRG);
		rdButtonGroup.add(rdbCertidao);
		etiquetaDocumento = new JLabel("Documento:");
		campoDocumento = new JFormattedTextField(formatadorCertidaoNascimento);
		campoDocumento.setDocument(new LetraMaiuscula());
		etiquetaTurmaAluno = new JLabel("Turma:");
		cmbTurmas = new JComboBox<String>(  new String[] {"vazio"} );
		pnBotoesAluno = new JPanel(new FlowLayout(FlowLayout.LEFT, 10, 5));
		pnBotoesAluno.setBounds(0, 270, 645, 30);
		pnBotoesAluno.setVisible(false);
		btnAlunoSalvar = new JButton("Salvar", new ImageIcon(imgSalvar));
		btnAlunoLimpar = new JButton("Limpar", new ImageIcon(imgLimpar));
		pnBotoesAluno.add(btnAlunoSalvar);
		pnBotoesAluno.add(btnAlunoLimpar);
		cabecalhoTabelaAlunos = new String[] {
				"matricula",
				"nome",
				"endereço",
				"telefone",
				"documento",
				"tipo",
				"código turma"};
		
		modeloTabelaAlunos = new DefaultTableModel(cabecalhoTabelaAlunos, 0);
		tabelaAlunos = new JTable( modeloTabelaAlunos ) {

			private static final long serialVersionUID = 1L;

			@Override
			public boolean isCellEditable(int row, int column) {
				return false;
			}
			
		};
		painelRolagemTabelaAlunos = new JScrollPane( tabelaAlunos );
		painelRolagemTabelaAlunos.setBounds(5, 305, 830, 205);
		painelRolagemTabelaAlunos.setVisible( false );
		menuExclusaoAluno = new JPopupMenu();
		itemRemoverAluno = new JMenuItem("remover aluno");
		itemRemoverAluno.setIcon( new ImageIcon( imgDeletar ) );
		menuExclusaoAluno.add(itemRemoverAluno);
		tabelaAlunos.setComponentPopupMenu(menuExclusaoAluno);
		
		/*adiciona items de gui ao painel de cadastro de alunos*/
		pnCadastroAluno.add(etiquetaCodigoAluno);
		pnCadastroAluno.add(campoCodigo);
		
		pnCadastroAluno.add(etiquetaNomeAluno);
		pnCadastroAluno.add(campoNomeAluno);
		
		pnCadastroAluno.add(etiquetaEndereco);
		pnCadastroAluno.add(campoEndereco);
		
		pnCadastroAluno.add(etiquetaTelefone);
		pnCadastroAluno.add(campoTelefone);
		
		pnCadastroAluno.add(etiquetaTipoDocumento);
		pnCadastroAluno.add(pnRadio);
		
		pnCadastroAluno.add(etiquetaDocumento);
		pnCadastroAluno.add(campoDocumento);
		
		pnCadastroAluno.add(etiquetaTurmaAluno);
		pnCadastroAluno.add(cmbTurmas);		
		
		
		/*fim painel cadastro*/
				
		
		/*adiciona componentes a janela*/
		add(painelCadastro);
		add(pnBotoes);
		add(pnBotoesAluno);
		add(painelRolagemTabela);
		add(painelRolagemTabelaAlunos);
	}	
	
	
	/* define eventos */
	private void tratarEventos() {
		
		
		btnLimpar.addActionListener( l-> {
			
			TurmaService.limparFormulario(campoTextoSala, campoNomeProfessor,
					campoTituloProfessor, campoDataEncerramento, 
					campoDataAbertura, campoCurso, campoCodigo);	
			
			btnSalvar.setEnabled(true);
			btnExcluir.setVisible(false);
			btnAtualizar.setVisible(false);
			repaint();
		});
		
		btnSalvar.addActionListener(l->{
			
			if(campoCodigo.getText().equals("")) {
				boolean resposta = TurmaService.salvarTurma(campoTextoSala, campoDataAbertura,
						campoDataEncerramento, campoNomeProfessor,
						campoTituloProfessor, campoCurso, campoCodigo);
				
				if(resposta) {
					
					painelCadastro.add("Alunos", pnCadastroAluno);
					
					atualizaTabelaEcomboBox();					
					
					TurmaService.limparFormulario(
							campoTextoSala,
							campoNomeProfessor,
							campoTituloProfessor,
							campoDataEncerramento,
							campoDataAbertura,
							campoCurso,
							campoCodigo
					);
					
					repaint();
				}
			}
									
		});
		
		btnAtualizar.addActionListener(l->{
			
			if(!campoCodigo.getText().equals(""))
			{
				boolean resposta = TurmaService.atualizarTurma(
						campoTextoSala,
						campoDataAbertura,
						campoDataEncerramento,
						campoNomeProfessor,
						campoTituloProfessor,
						campoCurso, campoCodigo);
				
				if(resposta) {
					atualizaTabelaEcomboBox();
					TurmaService.limparFormulario(
							campoTextoSala,
							campoNomeProfessor,
							campoTituloProfessor,
							campoDataEncerramento,
							campoDataAbertura,
							campoCurso,
							campoCodigo);
					
					repaint();
				}
			}else {
				JOptionPane.showMessageDialog(
						this.getRootPane(),
						"Por Favor click em um elemento da lista para selecioná-lo",
						"informação", JOptionPane.INFORMATION_MESSAGE);
			}
			
		});		
		
		this.tabelaTurmas.addMouseListener(new MouseListener() {
			
			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseClicked(MouseEvent e) {
				int linha_selecionda = tabelaTurmas.getSelectedRow();
				
				campoCodigo.setText(
						tabelaTurmas.getModel().getValueAt(linha_selecionda, 0).toString());
				
				campoCurso.setText(
						tabelaTurmas.getModel().getValueAt(linha_selecionda, 1).toString());
				
				campoTextoSala.setText(
						tabelaTurmas.getModel().getValueAt(linha_selecionda, 2).toString());
				
				campoDataAbertura.setText(
						tabelaTurmas.getModel().getValueAt(linha_selecionda, 3).toString());
				
				campoDataEncerramento.setText(
						tabelaTurmas.getModel().getValueAt(linha_selecionda, 4).toString());
				
				campoNomeProfessor.setText(
						tabelaTurmas.getModel().getValueAt(linha_selecionda, 5).toString());
				
				campoTituloProfessor.setText(
						tabelaTurmas.getModel().getValueAt(linha_selecionda, 6).toString());
				
				btnSalvar.setEnabled(false);
				btnExcluir.setVisible(true);
				btnAtualizar.setVisible(true);
				repaint();
			}
		});
		
			
		
		// carrega a lista de turmas e habilita painel de cadastro de alunos
		this.addWindowListener(new WindowAdapter() {
		
			// quando a janela for carregada carrega dados do bd
			@Override
			public void windowOpened(WindowEvent e) {
				SwingUtilities.invokeLater(()-> {
					
					turmas = TurmaService.carregarTurmas();
					
					if(!turmas.isEmpty()) {
						
						/**
						 * se possui turmas cadastradas adiciona painel de cadastro
						 * de alunos
						 * */
						painelCadastro.add("Alunos", pnCadastroAluno);
						//preencherComboBox(turmas);
						atualizaTabelaEcomboBox();
						preencherTabelaDeAlunos();
					}
					
					repaint();
					
				});
				
			}
			
		});
		
		/*exclusão de turma*/
		this.btnExcluir.addActionListener( l -> {
			// instanciar um objeto tipo turma
			Turma turmaSelecionada = new Turma();
			// obter dados do formulário e setar no objeto turma
			turmaSelecionada.setCodigo(campoCodigo.getText());
			turmaSelecionada.setCurso(campoCurso.getText());
			turmaSelecionada.setSala(campoTextoSala.getText());
			Professor professor = new Professor();
			professor.definirNome(campoNomeProfessor.getText());
			professor.setTitulacao(campoTituloProfessor.getText());
			turmaSelecionada.definirProfessor(professor);
			DateFormat formatadorData = new SimpleDateFormat("dd/MM/yyyy");
			Date dataAberturaTurma = null, dataFechamentoTurma = null;
			try {
				dataAberturaTurma = (Date) formatadorData.parse(campoDataAbertura.getText());
				dataFechamentoTurma = (Date) formatadorData.parse(campoDataEncerramento.getText());
			} catch (ParseException e1) {
				LogUtil.setApplicationLog(getClass().getName(), Level.WARNING, e1.getMessage());
			}
			turmaSelecionada.setDataAbertura(dataAberturaTurma);
			turmaSelecionada.setDataEncerramento(dataFechamentoTurma);	
			String[] opcoes = new String[]{"sim","não"};
			int resposta  = JOptionPane.showOptionDialog(
				this.getContentPane(),
				"Tem certeza que deseja remover essa turma?",
				"confirmação",JOptionPane.YES_NO_OPTION,JOptionPane.WARNING_MESSAGE,null,opcoes,1);
			
			
			// chamar a camada service e passar o objeto
			boolean foiDeletado = false;
			if(resposta == 0)
				foiDeletado = TurmaService.excluirTurma(turmaSelecionada);
			
			if(foiDeletado == true && resposta == 0) {
				
				JOptionPane.showMessageDialog(
						this.getRootPane(),
						"Registro removido com sucesso!",
						"aviso",
						JOptionPane.INFORMATION_MESSAGE);
				
				TurmaService.limparFormulario(
						campoTextoSala,
						campoNomeProfessor,
						campoTituloProfessor,
						campoDataEncerramento, 
						campoDataAbertura,
						campoCurso,
						campoCodigo);
				
				atualizaTabelaEcomboBox();
				
			}else {

				TurmaService.limparFormulario(
						campoTextoSala,
						campoNomeProfessor,
						campoTituloProfessor,
						campoDataEncerramento, 
						campoDataAbertura,
						campoCurso,
						campoCodigo);
			}
		});
		
		/*evento troca de abas*/
		this.painelCadastro.addChangeListener( l -> {
			/*obtem guia selecionada */
			int guiaSelecionada = this.painelCadastro.getSelectedIndex();
			switch(guiaSelecionada) {
				case 0:
					this.pnBotoes.setVisible(true);
					this.pnBotoesAluno.setVisible(false);
					this.painelRolagemTabela.setVisible(true);
					this.painelRolagemTabelaAlunos.setVisible(false);
				break;
				case 1:
					this.pnBotoes.setVisible(false);
					this.pnBotoesAluno.setVisible(true);
					this.painelRolagemTabela.setVisible(false);
					this.painelRolagemTabelaAlunos.setVisible(true);										
				break;
			}
			
			repaint();
				
		});
		
		/*mudar para rg*/
		this.rdbRG.addActionListener(l -> {
			
			//this.rdbRG.setSelected( true );
			
			this.campoDocumento.setText("");
			
			this.campoDocumento.setText(null);
			
			this.campoDocumento.setFormatterFactory(
					new DefaultFormatterFactory(formatadorRg));
			
			this.campoDocumento.setText(null);
				
			//repaint();
			
		});
		
		/*mudar para certidao nascimento*/
		this.rdbCertidao.addActionListener(l -> {
			
			this.rdbCertidao.setSelected(true);
			
			this.campoDocumento.setText("");
			
			this.campoDocumento.setFormatterFactory(
					new DefaultFormatterFactory(formatadorCertidaoNascimento));
			
			this.campoDocumento.setText(null);
			
			//repaint();
			
		});
		
		
		/*evento botao cadastrar aluno*/
		btnAlunoSalvar.addActionListener( l -> {
			
			// obtem o botao selecionado
			JRadioButton botaoSelecionado = rdbCertidao.isSelected() ?
					rdbCertidao : rdbRG;
			
			// obtem a turma selecionada
			String turmaSelecionada = this.cmbTurmas
					.getSelectedItem().toString();
			
			boolean salvoOuAtualizado = AlunoService.salvarAluno(
					campoCodigoAluno,
					campoNomeAluno,
					campoEndereco,
					botaoSelecionado,
					campoDocumento,
					campoTelefone,
					turmaSelecionada);
			
			if ( salvoOuAtualizado ) {				
				AlunoService.limparFormularioAluno(
						campoCodigoAluno,
						campoNomeAluno,
						campoEndereco,
						campoTelefone,
						botaoSelecionado,
						botaoSelecionado,
						campoDocumento,
						cmbTurmas,
						formatador,
						formatador1,
						formatadorTelefone,
						formatadorRg,
						formatadorCertidaoNascimento,
						this);
				
				preencherTabelaDeAlunos();
			}
			
		} );
		
		btnAlunoLimpar.addActionListener( l -> {
			
			AlunoService.limparFormularioAluno(
					campoCodigoAluno,
					campoNomeAluno,
					campoEndereco,
					campoTelefone,
					rdbCertidao,
					rdbRG,
					campoDocumento,
					cmbTurmas,
					formatador,
					formatador1,
					formatadorTelefone,
					formatadorRg,
					formatadorCertidaoNascimento, this);
			
		} );
		
		cmbTurmas.addActionListener( l -> {
			preencherTabelaDeAlunos();
		} );
		
		tabelaAlunos.addMouseListener(new MouseListener() {
			
			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
		
			@Override
			public void mouseClicked(MouseEvent e) {
				int linhaSelecionada = tabelaAlunos.getSelectedRow();
				
				campoCodigoAluno.setText(
						tabelaAlunos
						.getModel()
						.getValueAt(linhaSelecionada, 0).toString() );
				
				campoNomeAluno.setText(
						tabelaAlunos
						.getModel()
						.getValueAt(linhaSelecionada, 1).toString() );
				
				campoEndereco.setText(
						tabelaAlunos
						.getModel()
						.getValueAt(linhaSelecionada, 2).toString() );
				
				campoTelefone.setText(
						tabelaAlunos
						.getModel()
						.getValueAt(linhaSelecionada, 3).toString() );
				
				campoDocumento.setText(
						tabelaAlunos
						.getModel()
						.getValueAt(linhaSelecionada, 4).toString() );
				
				String tipoDocumento = tabelaAlunos
						.getModel()
						.getValueAt(linhaSelecionada, 5).toString();
				
				switch ( tipoDocumento ) {
				case "RG":
					rdbRG.setSelected(true);
					repaint();
					campoDocumento.setText("");
					
					campoDocumento.setFormatterFactory(
							new DefaultFormatterFactory(formatadorRg));
					
					campoDocumento.setText(
							tabelaAlunos
							.getModel()
							.getValueAt(linhaSelecionada, 4).toString());
					
					break;
				case "CERTIDAO_NASCIMENTO":
					rdbCertidao.setSelected(true);
					repaint();
					campoDocumento.setText("");
					
					campoDocumento.setFormatterFactory(
							new DefaultFormatterFactory(formatadorCertidaoNascimento));
					
					campoDocumento.setText(null);
					
					campoDocumento.setFormatterFactory(
							new DefaultFormatterFactory(formatadorCertidaoNascimento));
					
					campoDocumento.setText(
							tabelaAlunos
							.getModel()
							.getValueAt(linhaSelecionada, 4).toString());
					
					break;	
				default:
					break;
				}
				
				String codigoTurma = tabelaAlunos
						.getModel()
						.getValueAt(linhaSelecionada, 6)
						.toString();
				
				Turma turmaDoAluno = TurmaService
						.buscarTurmaPorId( codigoTurma );
				
				if ( turmaDoAluno != null ) {
					
					String dadosTurmaSelecionada = turmaDoAluno
							.getCurso() + " ; "	+ 
							turmaDoAluno.getProfessor().obterNome() + " ; "	+ 
							turmaDoAluno
							.obterDataAberturaConvertida(
									turmaDoAluno.getDataAbertura());
					
					cmbTurmas.setSelectedItem( dadosTurmaSelecionada );
					
				}
				
			}
		});		
		
		menuExclusaoAluno.addPopupMenuListener(new PopupMenuListener() {
			
			@Override
			public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
				
				SwingUtilities.invokeLater( () -> {
					
					linhaSelecionadaTabelaAlunos = tabelaAlunos
							.rowAtPoint(
							SwingUtilities
							.convertPoint(
									menuExclusaoAluno,
									new Point(0, 0),
									tabelaAlunos));					
					
						repaint();
				} );		
				
			}
			
			@Override
			public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void popupMenuCanceled(PopupMenuEvent e) {
				// TODO Auto-generated method stub
				
			}
		});
		
		itemRemoverAluno.addActionListener(l-> {
			
			int matriculaAluno = (int) tabelaAlunos
					.getModel().getValueAt(linhaSelecionadaTabelaAlunos, 0);
			
			String nomeAluno = (String) tabelaAlunos.getModel()
					.getValueAt(linhaSelecionadaTabelaAlunos, 1);
			
			String[] opcoes = new String[] {"sim","não"};
			
			boolean foiRemovidoBaseDados = false;
			
			int opcaoEscolhida = JOptionPane.showOptionDialog(
					getParent(),
					"Confirma a remoção do(a) aluno(a) "+nomeAluno+ " ?",
					"Confirmação",
					JOptionPane.DEFAULT_OPTION,
					JOptionPane.QUESTION_MESSAGE,
					null,
					opcoes,
					opcoes[1]);
			
			if ( opcaoEscolhida == 0 ) {
				
				// 1 remover o aluno da turma
				boolean alunoRemovidoTurma = AlunoService
						.removerAlunoDaTurma( matriculaAluno );
				
				// 2 remove o aluno do bd
				if ( alunoRemovidoTurma )
					foiRemovidoBaseDados = AlunoService
						.removerAluno( matriculaAluno );
				
				if ( alunoRemovidoTurma && foiRemovidoBaseDados ) {
					
					AlunoService.limparFormularioAluno(
							campoCodigo,
							campoNomeAluno,
							campoEndereco,
							campoTelefone,
							rdbCertidao,
							rdbRG,
							campoDocumento,
							cmbTurmas,
							formatador,
							formatador1,
							formatadorTelefone,
							formatadorRg,
							formatadorCertidaoNascimento,
							this);
					
					preencherTabelaDeAlunos();
					
					JOptionPane.showMessageDialog(
							getParent(),
							"Aluno removido com sucesso!",
							"Sucesso",
							JOptionPane.INFORMATION_MESSAGE);
					
				} else {
					
					JOptionPane.showMessageDialog(
							getParent(),
							"Falha ao remover Aluno",
							"Erro",
							JOptionPane.ERROR_MESSAGE);
					
					AlunoService.limparFormularioAluno(
							campoCodigo,
							campoNomeAluno,
							campoEndereco,
							campoTelefone,
							rdbCertidao,
							rdbRG,
							campoDocumento,
							cmbTurmas,
							formatador,
							formatador1,
							formatadorTelefone,
							formatadorRg,
							formatadorCertidaoNascimento,
							this);
				}
					
						
			}
			
			matriculaAluno = -1;
			nomeAluno = "";
			
		});	
	}
	
	
	private void atualizaTabelaEcomboBox() {
		
		DefaultTableModel modelo = 
				(DefaultTableModel) tabelaTurmas.getModel();
		
		List<Turma> listagem = TurmaService.carregarTurmas();
		
		modelo.setRowCount(0);//limpa a tabela
				
		listagem.forEach( t -> {	
			
			Object[] dadosTabela = {
					t.getCodigo(),
					t.getCurso(),
					t.getSala(),
					t.obterDataAberturaConvertida(t.getDataAbertura()),
					t.obterDataFechamentoConvertida(t.getDataEncerramento()),
					t.getProfessor().obterNome(),
					t.getProfessor().getTitulacao(),
					t.estaAberta() == true ? "Aberta":"fechada"
			};
			
			modelo.addRow(dadosTabela); // adiciona dados a tabela			
		});
		
		List<Turma> filtro = new ArrayList<Turma>();
				
		filtro = listagem
				.stream()
				.filter( t -> t.estaAberta() )
				.collect( Collectors.toList() );
		
		preencherComboBox( filtro );
	}
	
	private void preencherComboBox(List<Turma> turmas) {
		
		cmbTurmasDados = null;
		
		cmbTurmasDados = new String[ turmas.size() ];
		
		for ( int i = 0; i < turmas.size() ; i++ )
		{
			if( turmas.get(i) != null )
				cmbTurmasDados[i] = turmas.get(i).getCurso() 
				+ " ; " +turmas.get(i).getProfessor().obterNome()  
				+ " ; " + turmas.get(i)
				.obterDataAberturaConvertida(turmas.get(i).getDataAbertura());
			
		}
		
		if( cmbTurmasDados.length > 0 ) {
			cmbTurmas.removeAll();			
			cmbTurmas.setModel(new DefaultComboBoxModel<String>( cmbTurmasDados ));
		}
		
		repaint();
	}
	
	private void preencherTabelaDeAlunos() {
		
		String turmaSelecionada = ! cmbTurmas.getSelectedItem().toString().equals("") ? 
				cmbTurmas.getSelectedItem().toString(): "";
				
		if ( !turmaSelecionada.equals("") )
		{
			// buscar turma para obter lista de alunos
			String[] dadosTurma = turmaSelecionada.split(";");
			DateFormat formatadorData = new SimpleDateFormat("dd/MM/yyyy");
			Date dataInicioCurso = null;
			
			try {
				
				dataInicioCurso = formatadorData.
						parse( dadosTurma[2].trim() );
				
			}catch(ParseException e) {
				
				LogUtil.setApplicationLog(
						"JanelaPrincipal",
						Level.WARNING,
						e.getMessage());
			}
			
			Turma turma = TurmaService.obterTurma(
					dadosTurma[0].trim(),
					dadosTurma[1].trim(),
					dataInicioCurso);
			
			if ( turma != null ) {
				
				DefaultTableModel modeloTabelaAlunos = 
						( DefaultTableModel ) tabelaAlunos.getModel();
				
				modeloTabelaAlunos.setRowCount ( 0 );
				
				turma.getAlunos().forEach( aluno -> {
					
					Object[] dadosTabelaAlunos = {
							aluno.obterMatricula(),
							aluno.obterNome(),
							aluno.getEndereco(),
							aluno.getTelefone(),
							aluno.getDocumento(),
							aluno.getTipoDocumento(),
							turma.getCodigo()
					};
					modeloTabelaAlunos.addRow( dadosTabelaAlunos );					
				});
			}
			
		}	
	}
	
}


