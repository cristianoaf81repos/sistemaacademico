package cadastro.splash;

import java.awt.Toolkit;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JProgressBar;

public class Splash extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3374591742821036640L;
	public JLabel lbImageLogo;
	public JProgressBar progressBar = new JProgressBar();

	public Splash() {
		
		setIconImage(
				Toolkit
				.getDefaultToolkit()
				.getImage("src/main/java/cadastro/splash/icon32.png"));
		
		setBounds(0, 0, 402, 255);
		
		setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		
		setLocationRelativeTo(null);
		
		setLayout(null);
		
		setUndecorated(true);
		
		ImageIcon logo = new ImageIcon(
				Toolkit
				.getDefaultToolkit()
				.getImage("src/main/java/cadastro/splash/splash.png"));
		
		lbImageLogo = new JLabel(logo);
		lbImageLogo.setBounds(0, 0, 401, 254);
		add(lbImageLogo);
		progressBar.setBounds(0, 245, 402, 15);
		add(progressBar);

	}
}
