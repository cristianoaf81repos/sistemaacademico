package cadastro.repositorio;

import java.util.logging.Level;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import cadastro.modelo.Aluno;
import cadastro.util.JpaUtil;
import cadastro.util.LogUtil;

public class AlunoRepositorio {
	
	private EntityManager dao;
		
	
	public AlunoRepositorio() {
		
		this.dao = JpaUtil
				.getEntityManagerFactory()
				.createEntityManager();
		
	}
	
	private void garantirSessaoAberta() {
		if ( this.dao == null || !this.dao.isOpen() )
		
			this.dao = JpaUtil
			.getEntityManagerFactory()
			.createEntityManager();
		
	}
	
	private void garantirTransacao() {
		
		if( !this.dao.getTransaction().isActive() )
			this.dao.getTransaction().begin();
	}
	
	private void retrocederOperacaoEfechar() {
		this.dao.getTransaction().rollback();
		this.dao.close();
	}
	
	public Aluno salvarAluno( Aluno aluno ) {
		
		Aluno valorRetorno = null;
		String jpql = "from Aluno a where a.documento = :doc";
		
		try {
		
			garantirSessaoAberta();
			
			garantirTransacao();
			
			this.dao.persist(aluno);
			this.dao.getTransaction().commit();
									
			/*necessario abrir de novo*/
			garantirSessaoAberta();
			
			garantirTransacao();/*se necessário reiniciar a transacao*/
			
			TypedQuery< Aluno > consulta = this.dao.createQuery(jpql, Aluno.class);
			
			consulta.setParameter("doc", aluno.getDocumento());
			
			valorRetorno = consulta.getSingleResult();
			
			this.dao.close();
			
		} catch (Exception e) {
			
			retrocederOperacaoEfechar();
			
			LogUtil.setApplicationLog(
					"AlunoDAO",
					Level.WARNING,
					e.getMessage());
			
		}
		
		return valorRetorno;
	}
	
	public boolean atualizarDadosAluno( Aluno aluno ) {
		boolean valorRetorno = false;
		
		try {
			
			garantirSessaoAberta();
			
			garantirTransacao();
			
			this.dao.merge( aluno );
			
			this.dao.flush();
			
			this.dao.getTransaction().commit();
			
			this.dao.close();
			
			valorRetorno = true;
			
		} catch (Exception e) {
			
			retrocederOperacaoEfechar();
			
			LogUtil.setApplicationLog(
					"AlunoDAO",
					Level.WARNING,
					e.getMessage());
			
		}
		
		return valorRetorno;
	}
	
	
	public boolean removerAluno(int matricula) {
		
		boolean valorRetorno = false;
		
		try {
			
			garantirSessaoAberta();
			
			garantirTransacao();
			
			Aluno aluno = this.dao
					.find(Aluno.class, matricula);
			
			this.dao.remove( aluno );
			
			this.dao.flush();
			
			this.dao.getTransaction().commit();
			
			this.dao.close();
			
			valorRetorno = true;
			
		} catch (Exception e) {
			
			retrocederOperacaoEfechar();
			
			LogUtil.setApplicationLog(
					"AlunoDAO",
					Level.WARNING,
					e.getMessage());
		}
		
		return valorRetorno;
	}
}
