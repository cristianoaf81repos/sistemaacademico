package cadastro.repositorio;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import cadastro.modelo.Turma;
import cadastro.util.JpaUtil;
import cadastro.util.LogUtil;

public class TurmaRepositorio {
	
	private EntityManager dao;
	private String className = TurmaRepositorio.class.getName();

	public TurmaRepositorio() {
		this.dao = JpaUtil.getEntityManagerFactory().createEntityManager();
		
	}

	private void garantirSessaoAberta() {
		if(this.dao == null || !this.dao.isOpen())
			this.dao = JpaUtil.getEntityManagerFactory().createEntityManager();
	}	
	
	private void garantirTransacao() {
		if( !this.dao.getTransaction().isActive()	)
			this.dao.getTransaction().begin();
	}
	
	private void retrocederOperacaoEfechar() {
		this.dao.getTransaction().rollback();
		this.dao.close();
	}
	
	public boolean salvarTurma(Turma turma) {
		boolean valorRetorno = false;
		
		
		try {
			garantirSessaoAberta();
			
			garantirTransacao();
			
			// verifica se ja existe uma turma com mesmo prof e sala
			boolean jaFoiSalvo = checarSeJaNaoFoiSalvoAntes(
					turma.getProfessor().obterNome(),
					turma.getSala(),
					turma.getDataAbertura(),
					turma.getCurso());
			
			if(jaFoiSalvo) {
				
				return valorRetorno = false;
				
			} else {
				
				garantirSessaoAberta();
				
				garantirTransacao();				
				
				this.dao.persist(turma);
				
				this.dao.getTransaction().commit();
				
				this.dao.close();
				
				valorRetorno = true;
			}
			
		} catch (Exception e) {
			
			retrocederOperacaoEfechar();
			
			LogUtil.setApplicationLog(
					className,
					Level.WARNING,
					e.getMessage());			
		}
		
		return valorRetorno;
	}
	
	
	public boolean atualizarTurma(Turma turma) {
		
		boolean valorRetorno = false;
		
		try {
		
			garantirSessaoAberta();
			garantirTransacao();
			
			Turma turmaAtualizacao = this.dao
					.find(Turma.class, turma.getCodigo());
			
			if(turmaAtualizacao != null 
					&& turmaAtualizacao
					.getCodigo()
					.equals(turma.getCodigo())) {
				
				turmaAtualizacao
				.definirProfessor(turma.getProfessor());
				
				turmaAtualizacao.setCurso(turma.getCurso());
				
				turmaAtualizacao
				.setDataAbertura(turma.getDataAbertura());
				
				turmaAtualizacao
				.setDataEncerramento(turma.getDataEncerramento());
				
				turmaAtualizacao.setSala(turma.getSala());				
				
				this.dao.getTransaction().commit();
				
				this.dao.close();
				
				valorRetorno = true;
			}
				
		} catch (Exception e) {
			
			retrocederOperacaoEfechar();
			
			LogUtil.setApplicationLog(
					className,
					Level.WARNING,
					e.getMessage());
			
		}
		
		return valorRetorno;
	}
	
	public boolean checarSeJaNaoFoiSalvoAntes(String professor, String sala,
			Date data, String curso) {
		
		boolean valorRetorno = false;
		
		List<Turma> turmas = new ArrayList<Turma>();
		
		Turma checarSeExiste = null;
				
		try {
			garantirSessaoAberta();
			
			garantirTransacao();
			
			TypedQuery<Turma> consulta = this.dao
					.createQuery("from Turma", Turma.class);
			
			turmas = consulta.getResultList();	
			
			if(!turmas.isEmpty())
				for(Turma t : turmas)
				{
					if(t!=null) {
						
						if(t.getSala().equals(sala.trim()) 
								&& t.getProfessor()
								.obterNome().equals(professor) 
								&& t.getDataAbertura().equals(data)
								&& t.getCurso().equals(curso))
							
							checarSeExiste = t;
						
					}						
					
				}
			
			if(checarSeExiste != null)
				valorRetorno = true;
			
			this.dao.close();
			
		} catch (Exception e) {
			
			retrocederOperacaoEfechar();
			
			LogUtil.setApplicationLog(
					className,
					Level.WARNING,
					e.getMessage());
			
		} 
		
		return valorRetorno;
	}
	
	public List<Turma> carregarTurmas() {
		
		List<Turma> turmas = new ArrayList<Turma>();
		
		try {
			
			garantirSessaoAberta();
			
			garantirTransacao();
			
			TypedQuery<Turma> consulta = this.dao
					.createQuery("from Turma", Turma.class);
			
			turmas = consulta.getResultList();	
			
			turmas.get(0).getProfessor().obterNome();
			
			this.dao.close();
			
		} catch (Exception e) {
			
			retrocederOperacaoEfechar();
			
			LogUtil.setApplicationLog(
					className,
					Level.WARNING,
					e.getMessage());
			
		}
		
		return turmas;
	}
	
	public boolean deletarTurma(String codigo) {
		
		boolean valorRetorno = false;
		
		try {
			
			garantirSessaoAberta();
			
			garantirTransacao();
			
			Turma turmaRemocao = this.dao.find(Turma.class, codigo);
			
			this.dao.remove(turmaRemocao);
			
			this.dao.getTransaction().commit();
			
			this.dao.close();
			
			valorRetorno = true;
			
		} catch (Exception e) {
			
			retrocederOperacaoEfechar();
			
			LogUtil.setApplicationLog(
					className,
					Level.WARNING,
					e.getMessage());
			
		}
		return valorRetorno;
	}
	
	
	public Turma obterTurma(String curso, String professor,
			Date dataInicioCurso) {
		
		Turma turma = null;
		
		try {
			
			garantirSessaoAberta();
			garantirTransacao();
			
			String jpql = "from Turma t where t.curso ="
					+ " :curso and t.dataAbertura = :data";
			
			TypedQuery< Turma > turmas = this.dao
					.createQuery(jpql, Turma.class);
			
			turmas.setParameter("curso", curso);
			
			turmas.setParameter("data", dataInicioCurso);
			
			turma = turmas.getResultList()
			.stream()
			.filter( t -> t.getProfessor().obterNome().equals( professor ) )
			.findAny().orElse( null );
			
			this.dao.close();
			
		} catch (Exception e) {
			
			retrocederOperacaoEfechar();
			
			LogUtil.setApplicationLog(
					"TurmaDAO",
					Level.WARNING,
					e.getMessage());
			
		}
		
		return turma;
	}
	
	public boolean atualizarTurmaComAlunos (Turma turma) {
		
		boolean valorRetorno = false;
		
		try {
			garantirSessaoAberta();
			
			garantirTransacao();
			
			this.dao.merge(turma);
			
			this.dao.flush();
			
			this.dao.getTransaction().commit();
			
			this.dao.close();
			
			valorRetorno = true;
			
		} catch (Exception e) {
			
			retrocederOperacaoEfechar();
			
			LogUtil.setApplicationLog(
					"TurmaDAO",
					Level.WARNING,
					e.getMessage());
		}
		return valorRetorno;
	}
	
	public Turma obterTurmaPorId( String oid ) {
		Turma valorRetorno = null;
		
		try {
		
			garantirSessaoAberta();
			
			garantirTransacao();
			
			valorRetorno = this.dao.find( Turma.class, oid );
			
			this.dao.close();
			
		} catch (Exception e) {
			
			retrocederOperacaoEfechar();
			
			LogUtil.setApplicationLog(
					"TurmaDAO",
					Level.WARNING,
					e.getMessage());
			
		}
		
		return valorRetorno;
	}
}
