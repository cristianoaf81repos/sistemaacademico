package cadastro.util;

import java.util.logging.Level;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class JpaUtil {
	private static final String NOME_PU = "CADASTROPU";
	private static EntityManagerFactory emf = null;
	private static String className = JpaUtil.class.getName();
	
	public static EntityManagerFactory getEntityManagerFactory() {
		
		try {
			
			if(emf == null)
				emf = Persistence.createEntityManagerFactory(NOME_PU);
			
		} catch (Exception e) {
			
			LogUtil.setApplicationLog(className, Level.WARNING, e.getMessage());
		}
		
		return emf;
	}

	
	public static void fecharConexao() {
		try {
			
			if(emf != null)
				emf.close();
			
		} catch (Exception e) {
			LogUtil.setApplicationLog(className, Level.WARNING, e.getMessage());
		}
	} 
}
