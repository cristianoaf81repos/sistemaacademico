package cadastro.util;

import java.util.logging.Level;
import java.util.stream.IntStream;

import javax.persistence.EntityManager;

import cadastro.principal.PrincipalClass;
import cadastro.splash.Splash;

public class InicializacaoUtil {
	
	public static void mostrarLogoNoInicio() {
		try {
			LookAndFeelUtil.definirVisualdaAplicação();
			Splash splash = new Splash();
			splash.setVisible(true);
			
			/*truque para inicializar jpa*/
			EntityManager manager = JpaUtil
					.getEntityManagerFactory()
					.createEntityManager();
			
			manager.getTransaction().begin();
			
			manager.close();
			
			IntStream.range(0, 100).forEach( i -> {
				try {
					Thread.sleep(30);				
					splash.progressBar.setValue(i);
				} catch (InterruptedException e) {
					LogUtil.setApplicationLog(
							PrincipalClass.class.getName(), Level.WARNING, e.getMessage());
				}
				
			});
			splash.setVisible(false);			
		} catch (Exception e) {
			LogUtil.setApplicationLog(
					PrincipalClass.class.getName(), Level.WARNING, e.getMessage());
		}
	}
}
