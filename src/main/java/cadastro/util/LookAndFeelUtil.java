package cadastro.util;

import java.awt.Insets;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.logging.Level;

import javax.swing.UIManager;

import com.formdev.flatlaf.IntelliJTheme;

public class LookAndFeelUtil {
	
	private static String className = LookAndFeelUtil.class.getName();
	
	public static void definirVisualdaAplicação() {
		try {
			// valores vão de zero a 999
			UIManager.put( "Button.arc", 000 );//curvatura do botao
			UIManager.put( "Component.arc", 000 );// curvatura componentes
			UIManager.put( "ProgressBar.arc", 000 );//curvatura barra progresso
			UIManager.put( "TextComponent.arc", 000 );// curvatura caixa de texto
			UIManager.put( "ScrollBar.showButtons", true );/*mostra barra de rolagem*/
			UIManager.put( "Component.arrowType", "chevron" );/*tipo de botao da barra rolagem*/
			UIManager.put( "ScrollBar.thumbInsets", new Insets( 2, 2, 2, 2 ) );/*larg barra rolagem*/
			UIManager.put( "ScrollBar.showButtons", true );/*mostrar botoes barra rolagem*/
			
			//UIManager.setLookAndFeel("org.netbeans.swing.laf.dark.DarkNimbusLookAndFeel");// metal Looks good!
			//UIManager.setLookAndFeel(new FlatLightLaf());
			/*IntelliJTheme.install(
			JanelaPrincipal.class.getResourceAsStream("/themes/HiberbeeDark.theme.json"));*/
			/*IntelliJTheme.install(
			JanelaPrincipal.class.getResourceAsStream("/themes/DarkPurple.theme.json"));*/
			
			InputStream is = new FileInputStream(new File("src/main/resources/themes/arc-theme-orange.theme.json"));
			
			/*IntelliJTheme.install(
					JanelaPrincipal.class.getResourceAsStream("/themes/arc-theme-orange.theme.json"));*/
			IntelliJTheme.install(is);
			
			
			
		} catch (Exception e) {
			
			LogUtil.setApplicationLog(className, Level.WARNING, e.getMessage());
			
			// caso falhe a definicao do visual carrega o look padrao metal
			try {
				UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
			} catch (Exception e2) {
				
				LogUtil.setApplicationLog(className, Level.WARNING, e.getMessage());
				
			}
		}
	}
	
}
