package cadastro.util;

import java.util.logging.Level;
import java.util.logging.Logger;

public class LogUtil {
	
	public static void setApplicationLog(String className, Level level, String msg) {
		Logger log = Logger.getLogger(className);
		log.log(level, msg);
	}

}
